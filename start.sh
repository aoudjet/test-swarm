#!/bin/bash
chmod 600 gitserver.pem
/var/lib/jenkins/.local/bin/aws ec2 describe-instances --query "Reservations[*].Instances[*].PublicIpAddress" --output=text > ntt.txt
/var/lib/jenkins/.local/bin/aws cloudformation create-stack --stack-name ntt-ee-test --template-url https://s3.amazonaws.com/nttdatapps/ntt/docker.template --capabilities CAPABILITY_IAM --region eu-west-1
sleep 250
/var/lib/jenkins/.local/bin/aws ec2 describe-instances --query "Reservations[*].Instances[*].PublicIpAddress" --output=text > ips.tmp.txt
IP=$(awk 'NR==FNR{a[$0]=1;next}!a[$0]' ntt.txt ips.tmp.txt)
sleep 120
echo "You Manager IP address is: " $IP
echo "You cannot login to the worker, use the manager as a jump host\n"
ssh -tt -o StrictHostKeyChecking=no -i gitserver.pem docker@$IP <<'ENDSSH'
echo "getting the manager ready"
sleep 120
echo "
alias git='docker run -it --rm --name git -v $PWD:/git -w /git naoudjet/dashboard git'
git version
git clone https://aoudjet:vico999999@gitlab.com/aoudjet/test-swarm.git
sudo chown -R $USER test-swarm
cd test-swarm
docker stack deploy -c docker-stack.yml dev
" > docker
chmod 755 docker
./docker && exit
ENDSSH
dns=$(/var/lib/jenkins/.local/bin/aws cloudformation describe-stacks --stack-name ntt-ee-te | grep ntt-ee-da-external | sed 's/\"//g' | sed 's/,//g' | awk '{print $2}')
echo "1- Access Manager:"
echo "http://$dns:9999"
